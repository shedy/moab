<?php
/**
 * Created by PhpStorm.
 * User: MI
 * Date: 26.12.2018
 * Time: 16:37
 */

namespace Wikilect\MoabWordstat;


class MoabWordstatUils
{

    /**
     * @param $url
     * @return bool|string
     * @throws MoabWordstatException
     */
    public static function getUnzipGetCsvFile($url)
    {
        // косяк с редиректом https
        $url = self::fixUrl($url);
        $tempdir = static::tempdir();
        $filename = $tempdir . '/' . 'moab_tmp.zip';

        $data = file_get_contents($url);
        file_put_contents($filename, $data);

        $zip = new \ZipArchive;
        $res = $zip->open($filename);
        if ($res === TRUE) {
            $zip->extractTo($tempdir);
            $zip->close();
            $files = scandir($tempdir);
            foreach ($files as $name) {

                if (strpos($name, '.csv')) {
                    $str = file_get_contents($tempdir . '/' . $name);
                    static::delTree($tempdir);
                    return $str;
                }
            }
            throw new MoabWordstatException('can not find csv in zip');

        }

        throw new MoabWordstatException('can not unzip file');

    }

    /**
     * @param $url
     * @return mixed
     */
    private static function fixUrl($url)
    {
        $url = str_replace('http://', 'https://', $url);
        preg_match('!https\://tools\.moab\.pro/Downloads/[\d]*/[_]{0,1}(.*)[_]{0,2}(.*?).zip!si', $url, $results);
        $url = str_replace($results[1], urlencode($results[1]), $url);
        return $url;

    }

    /**
     * @return bool|string
     * @throws MoabWordstatException
     */
    private static function tempdir()
    {
        $tempfile = tempnam(sys_get_temp_dir(), '');
        if (file_exists($tempfile)) {
            unlink($tempfile);
        }
        if (!mkdir($tempfile) && !is_dir($tempfile)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $tempfile));
        }
        if (is_dir($tempfile)) {
            return $tempfile;
        }
        throw new MoabWordstatException('can not create temporary directory for moab');
    }

    /**
     * @param $dir
     * @return bool
     */
    private static function delTree($dir): bool
    {
        $files = array_diff(scandir($dir, SCANDIR_SORT_NONE), array('.', '..'));
        foreach ($files as $file) {
            is_dir("$dir/$file") ? self::delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    /**
     * @param $content
     * @return array
     */
    public  static function parseCsv($content): array
    {
        $result = [];
        $lines = explode("\n", $content);

        foreach ($lines as $count => $line) {
            if ($count === 0) {
                continue;
            }
            $row = str_getcsv($line, ';');
            if (trim($row[0]) !== '') {
                $result[$row[0]] = $row[1];
            }


        }
        return $result;
    }

}